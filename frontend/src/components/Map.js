import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';
import React from 'react'

export class MapContainer extends React.Component {

    constructor(props) {
        super(props);

        var geocoder = new this.props.google.maps.Geocoder();

        var This = this;
        this.state = { 
            contacts: this.props.contacts, 
            activeMarker: null, 
            showingInfoWindow: false,
            selectedPlace: {}
        };

        this.state.contacts.map((contact, key) => {
            geocoder.geocode({ 'address': contact.address }, function (results, status) {
                if (status === 'OK') {
                    var contacts = This.state.contacts;
                    contacts[key].position = results[0].geometry.location;
                    This.setState({ contacts });
                }
            });
        });
    }

    onMarkerClick = (props, marker, e) =>
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });

    onMapClicked = (props) => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            })
        }
    };

    render() {
        return (
            <Map google={this.props.google}
                zoom={12}
                style={{ width: '90%', height: '90%' }}
                center={{ lat: -36.862163, lng: 174.765502 }}
                onClick={this.onMapClicked}>

                {this.state.contacts.map((contact, key) =>
                    <Marker onClick={this.onMarkerClick} key={key} name={contact.name} id={contact.id} title={contact.name} position={contact.position} />
                )}

                <InfoWindow
                    marker={this.state.activeMarker}
                    visible={this.state.showingInfoWindow}>
                    <div>
                        <a href={`/contacts/${this.state.selectedPlace.id}/`}>{this.state.selectedPlace.name}</a>
                    </div>
                </InfoWindow>
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: "AIzaSyCf35GWjp1RV-dAPpho1hbbUUVjtV_x6Ok"
})(MapContainer)