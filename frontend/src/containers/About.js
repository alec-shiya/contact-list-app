import React from 'react'
import { withSiteData } from 'react-static'
//
import logoImg from '../logo.png'

export default withSiteData(() => (
  <div>
    <h1>Implementation details</h1>

    <p>App consists of 2 components:
      <br />
      1. the <b>Frontend</b>, a static single page app built in React and hosted on Azure blob storage
      <br />
      2. a <b>Function</b> app used to retrieve a list of contacts
    </p>

    <hr/>

    <h4>the Frontend</h4>
    <p>
      Built with react-static templates, which has some pretty cool aspects - all data and routes are loaded upfront, so navigation once the page laods is *fast*.
      <br />
      No server needed.
    </p>

    <hr/>

    <h4>the Function</h4>
    <p>
      Really basic function app, with a single endpoint to load contact list.
      <br />
      The contacts are hard-coded but can just as well come from a json file in blob storage or a table in a document DB.
    </p>

    <hr/>

    <h4>Build Process</h4>
    <p>
      Since the repo is in Bitbucket its built and deployed using a Bitbucket pipeline.
      <br />
      Setup for trunk based development. Code is developed in short-lived branches and submitted as a PR to master.
      <br />
      Pipeline kicks in to compile and run tests on the PR.
      <br />
      If tests pass, PR can be merged to master, and pipeline continues to deploy.
      <br />
      <br />
      Big downside here is that Frontend and Function are deployed together on any merge to master.
      <br />
      Bitbucket's pipeline doesn't allow triggering different steps depending on which subdirectory is modified.
    </p>
    
    <hr/>

    <h4>Scalability</h4>
    <p>
      The solution is made up of Hot Storage and a Function App. Should scale to infinity :)
    </p>
  </div>
))
