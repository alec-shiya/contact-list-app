import React from 'react'
import { withRouteData, Link } from 'react-static'
//

export default withRouteData(({ contact }) => (
  <div>
    <Link to="/contacts">{'<'} Back to Contact List</Link>
    <br />
    <img src={contact.businessCardUrl} alt={contact.name} width={'500px'} style={{margin:'30px'}} />
  </div>
))
