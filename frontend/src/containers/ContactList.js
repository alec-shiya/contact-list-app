
import React, { Component } from 'react'
import { withRouteData, Link } from 'react-static'

export default withRouteData(({ contacts }) => {
  return <ContactFilter contacts={contacts}></ContactFilter>
})

class ContactFilter extends Component {
  state = {
    contacts: this.props.contacts,
    input: ''
  }

  onChangeHandler(e) {
    this.setState({
      input: e.target.value,
    })
  }

  // return list of contacts filtered by the given text
  filterContactsByText(list, text) {
    if (!text) return list;
  
    var filterText = text.toLowerCase();
  
    return list.filter(contact =>
      contact.name.toLowerCase().includes(filterText)
    );
  }

  // group list of contacts by their name
  groupByName(list) {
    return list.reduce(function (rv, x) {
      (rv[x.name.charAt(0)] = rv[x.name.charAt(0)] || []).push(x);
      return rv;
    }, {});
  };

  render() {
    var groups = this.groupByName(this.filterContactsByText(this.state.contacts, this.state.input));

    return <div>
      <h1>Contacts</h1>

      <div className='filter'>
        <label>Filter Contacts</label>
        <input value={this.state.input} type='text' onChange={this.onChangeHandler.bind(this)} />
      </div>

      {Object.keys(groups).sort().map(key => (
        <div key={key}>
          {key} ({groups[key].length})
          <ul>
            {
              groups[key].map(contact => (
                <li key={contact.id}>
                  <Link to={`/contacts/${contact.id}/`}>{contact.name}</Link>
                </li>
              ))
            }
          </ul>
          <br />
        </div>
      ))}
    </div>
  }
}