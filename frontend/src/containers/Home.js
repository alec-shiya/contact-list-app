import React from 'react'
import { withSiteData } from 'react-static'
//
import logoImg from '../logo.png'

export default withSiteData(() => (
  <div>
    <h1 style={{ textAlign: 'center' }}>Welcome to</h1>
    <img src={logoImg} alt='' width='300px' height='300px' style={{ display: 'block', margin: '0 auto' }} />
    <h1 style={{ textAlign: 'center' }}>the Contact List App</h1>
    <h3 style={{ textAlign: 'center', fontStyle: 'underline' }}>&copy; 2018 Alec Shiya</h3>
  </div>
))
