import React from 'react'
import { withRouteData, Link } from 'react-static'
import MapContainer from '../components/Map'

export default withRouteData(({ contacts }) => (
  <MapContainer contacts={contacts}>
  </MapContainer>
))