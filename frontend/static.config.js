import axios from 'axios'

export default {
  getSiteData: () => ({
    title: 'Contact List',
  }),
  getRoutes: async () => {
    const { data: contacts } = await axios.get('https://contact-list-app-functions.azurewebsites.net/api/GetContactList?code=JohG9q6NNTyesjHPnpjPmpVy9KafkX8ADrHSTjrImogWbCQOrtSwUA==')
    return [
      {
        path: '/',
        component: 'src/containers/Home',
      },
      {
        path: '/contacts',
        component: 'src/containers/ContactList',
        getData: () => ({
          contacts,
        }),
        children: contacts.map(contact => ({
          path: `/${contact.id}`,
          component: 'src/containers/Contact',
          getData: () => ({
            contact,
          }),
        })),
      },
      {
        path: '/map',
        component: 'src/containers/Map',
        getData: () => ({
          contacts,
        }),
      },
      {
        path: '/about',
        component: 'src/containers/About',
      },
      {
        is404: true,
        component: 'src/containers/404',
      },
    ]
  },
}
