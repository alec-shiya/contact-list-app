﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Contact_List_App_Functions.Models;
using System.Linq;

namespace Contact_List_App_Functions.Tests
{
    [TestClass]
    public class GetContactListTests
    {
        [TestMethod]
        public void GetContactListShouldReturnExpectedData()
        {
            var personList = new ContactListService().GetContactList().ToList();

            Assert.AreEqual(4, personList.Count);
            Assert.AreEqual("James McGill", personList[0].Name);
            Assert.AreEqual("Bacon", personList[1].LastName);
            Assert.AreEqual("neilarmstrong@aol.com", personList[2].EmailAddress);
            Assert.AreEqual(4, personList[3].Id);
        }
    }
}
