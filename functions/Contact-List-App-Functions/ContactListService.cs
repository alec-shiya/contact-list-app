﻿using System;
using System.Collections.Generic;
using System.Text;
using Contact_List_App_Functions.Models;

namespace Contact_List_App_Functions
{
    public class ContactListService
    {
        public IEnumerable<Person> GetContactList()
        {
            return new Person[] {
                new Person() {
                    Id = 1,
                    FirstName = "James",
                    LastName = "McGill",
                    PhoneNumber = "(567) 458-9875",
                    EmailAddress = "james.mcgill@hotmail.com",
                    Address = "445 Mount Eden Rd, Mount Eden, Auckland 1024",
                    URL = "www.jamesmcgill.com",
                    BusinessCardUrl = "https://contactlistappstorage.blob.core.windows.net/images/james-mcgill.jpg" },
                new Person() {
                    Id = 2,
                    FirstName = "Kevin",
                    LastName = "Bacon",
                    PhoneNumber = "(798) 332-5243",
                    EmailAddress = "kevin@bacon.com",
                    Address = "45-7 Pine St, Mount Eden, Auckland 1041",
                    URL = "www.kevinbacon.com",
                    BusinessCardUrl = "https://contactlistappstorage.blob.core.windows.net/images/kevin-bacon.jpg"  },
                new Person() {
                    Id = 3,
                    FirstName = "Neil",
                    LastName = "Armstrong",
                    PhoneNumber = "(124) 423-5543",
                    EmailAddress = "neilarmstrong@aol.com",
                    Address = "25 Levene Pl, Mount Wellington, Auckland 1060",
                    URL = "www.moonlanding.com",
                    BusinessCardUrl = "https://contactlistappstorage.blob.core.windows.net/images/neil-armstrong.jpg"  },
                new Person() {
                    Id = 4,
                    FirstName = "Neil",
                    LastName = "deGrasse Tyson",
                    PhoneNumber = "(801) 999-5543",
                    EmailAddress = "neiltyson@space.com",
                    Address = "670 Manukau Rd, Epsom, Auckland 1345",
                    URL = "www.astrophysics.com",
                    BusinessCardUrl = "https://contactlistappstorage.blob.core.windows.net/images/neil-tyson.jpg"  }
            };
        }
    }
}
