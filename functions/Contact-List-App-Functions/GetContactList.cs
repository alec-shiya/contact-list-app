﻿
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using Contact_List_App_Functions.Models;

namespace Contact_List_App_Functions
{
    public static class GetContactList
    {
        [FunctionName("GetContactList")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequest req, ILogger log)
        {
            log.LogInformation("GetContactList triggered.");

            var contactService = new ContactListService();

            return new OkObjectResult(contactService.GetContactList());
        }
    }
}
