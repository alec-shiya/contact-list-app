﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contact_List_App_Functions.Models
{
    public class Person
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string EmailAddress { get; set; }

        public string URL { get; set; }

        public string Address { get; set; }

        public string BusinessCardUrl { get; set; }

        public string Name { get => $"{FirstName} {LastName}"; }
    }
}
